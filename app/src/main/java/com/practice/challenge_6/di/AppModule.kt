package com.practice.challenge_6.di

import com.practice.challenge_6.api.APIClient
import com.practice.challenge_6.database.cart.CartDatabase
import com.practice.challenge_6.database.cart.CartRepository
import com.practice.challenge_6.database.profile.ProfileDatabase
import com.practice.challenge_6.viewmodel.cart.CartViewModel
import com.practice.challenge_6.viewmodel.profile.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModule {
    private val appModule = module {
        single { CartDatabase.getDatabase(context = get()) }
        single { ProfileDatabase.getDatabase(context = get()) }

        single { APIClient.instance }
    }

    private val repoModule = module{
        single {CartRepository(get())}
    }

    private val viewModelModule = module{
        viewModelOf(::CartViewModel)
        viewModelOf(::ProfileViewModel)
    }

    val listModule: List<Module> = listOf(
        appModule,
        repoModule,
        viewModelModule
    )

}