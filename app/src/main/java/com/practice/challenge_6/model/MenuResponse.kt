package com.practice.challenge_6.model


import com.google.gson.annotations.SerializedName

data class MenuResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<MenuData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)